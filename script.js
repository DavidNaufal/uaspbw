let main = document.getElementById('main');
let floating_button = document.getElementById('floating_button');
let main_bg = document.getElementById('main_bg');
let addlist_form = document.getElementById('addlist_form');
let root = document.getElementById('root');
let subtitle = document.getElementById('subtitle');

// tambahkan tanggal ke subtitle
subtitle.innerHTML = new Date().toLocaleDateString();

// data list belanja
let data_list_belanja = [];

// tombol data list belanja
floating_button.addEventListener('click', function (event) {
  if (main.style.display == 'flex') {
    hidden_main();
  } else {
    show_main();
  }
});
main_bg.addEventListener('click', function (event) {
  hidden_main();
});

function show_main() {
  main.style.display = 'flex';
  floating_button.style.backgroundColor = 'gray';
  floating_button.style.transform = 'rotate(45deg)';
}

function hidden_main() {
  main.style.display = 'none';
  floating_button.style.backgroundColor = '#0A2647';
  floating_button.style.transform = 'rotate(0deg)';
}

// Tombol submit
addlist_form.addEventListener('submit', function (event) {
  event.preventDefault();
  let barang = event.target.barang.value;
  let jumlah = event.target.jumlah.value;
  let index = event.target.indexArray.value.trim();

  // edit data
  // console.info(index);
  if (index.length != 0) {
    data_list_belanja[index].nama_barang = barang;
    data_list_belanja[index].jumlah_barang = jumlah;
    clearValue(event);
    hidden_main();
    renderToHtml();
    return;
  }

  //push
  data_list_belanja.push({
    nama_barang: barang,
    jumlah_barang: jumlah,
    tanggal: new Date().toLocaleDateString(),
  });

  clearValue(event);
  hidden_main();
  renderToHtml();
});

function clearValue(event) {
  event.target.barang.value = '';
  event.target.jumlah.value = '';
  event.target.indexArray.value = '';
}

function renderToHtml() {
  root.innerHTML = '';
  data_list_belanja.forEach((data, index) => {
    root.innerHTML += `<div class="card"> 
    <small> ${data.tanggal}</small>  
    <div> ${data.nama_barang} <span>${(data.jumlah_barang)}</span></div>
    <div class="buttonCard">
    <button onclick="handleEdit(${index})">Edit</button>
    <button onclick="handleDelete(${index})">Hapus</button>
    <div
    </div>`;
  });
}

function handleDelete(index) {
  data_list_belanja.splice(index, 1);
  renderToHtml();
}

function handleEdit(index) {
  document.getElementById('barang').value =
    data_list_belanja[index].nama_barang;
  document.getElementById('jumlah').value =
    data_list_belanja[index].jumlah_barang;
  document.getElementById('indexArray').value = index;
  show_main();
  // set value
}